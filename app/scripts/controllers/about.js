'use strict';

/**
 * @ngdoc function
 * @name optpanelApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the optpanelApp
 */
angular.module('optpanelApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
