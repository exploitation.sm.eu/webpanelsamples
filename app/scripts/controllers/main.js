'use strict';

/**
 * @ngdoc function
 * @name optpanelApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the optpanelApp
 */
angular.module('optpanelApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
