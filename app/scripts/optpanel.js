'use strict';

var optpanel = angular.module('optpanel', ['xuc.services'])
 .config(function($locationProvider) { $locationProvider.html5Mode(true); });

  optpanel.controller('queueController',
    ['XucQueue','$scope','$timeout','$filter','$location',
    function(xucQueue, $scope, $timeout, $filter, $location) {
    var updateQueueFrequency = 5;
    $scope.queueFilter = $location.search().filter;

    $scope.queues = [];

    $scope.getQueues = function() {
        xucQueue.updateQueues(updateQueueFrequency);
        $scope.queues = $filter('filter')(xucQueue.getQueues(), { name : $scope.queueFilter});
        $timeout($scope.getQueues,updateQueueFrequency*1000,false);
        $scope.$digest();
    };
    $timeout($scope.getQueues,1000,false);


    $scope.$on('ctiLoggedOn', function() {
        xucQueue.start();
    });
    $scope.$on('linkDisConnected', function() {
        console.log('queueController linkDisConnected');
        $scope.queues = [];
    });

  }]);
optpanel.factory('ArrDispatcher',[function(){

  var dispatched = [];
  var rec = 0;

  var consoleAgents= function(agents) {
    console.log('agents : '+ agents.length);

    angular.forEach(agents, function(agent, key) {
      console.log(key + ' ' + agent.firstName+ ' ' + agent.phoneNb);
    });
  };

  var initDispatch = function(nbLines) {
    dispatched = [];
    for (var i=0; i < nbLines; i++ ) {
      dispatched.push([]);
    }
  };
  var getNbLines = function(nbOfElt, nbOfCol) {
    var nbLines = Math.floor((nbOfElt/nbOfCol));
    //console.log(nbOfElt+': ' +nbLines + ' ' + nbOfElt % nbOfCol);
    if (( nbOfElt % nbOfCol) > 0) {
      nbLines += 1;
    }
    return nbLines;
  };

  var doDispatch = function(arr,nbOfCol) {
    rec = rec + 1;
    if (rec > 10000) {
      console.error('maximum recursive('+ rec +') attempt to dispatch agents nb of col : '+ nbOfCol);
      consoleAgents(arr);
      return;
    }
    if (arr.length == 0) return;

    var nbLines = getNbLines(arr.length, nbOfCol);
    for (var i = 0; i < nbLines; i++) {
      dispatched[i].push(arr[i]);
    }
    arr.splice(0,nbLines);
    doDispatch(arr,nbOfCol-1);
  };

  var _dispatch = function(arr, nbOfCol) {
    rec = 0;
    var nbLines = getNbLines(arr.length, nbOfCol);
    initDispatch(nbLines);

    doDispatch(arr,nbOfCol);
    //console.log('-----------'+rec);
    return dispatched;
  };

  return {
    dispatch : _dispatch
  }
}]);

optpanel.factory('thresholdStyles',[function(){

  var defaultStyle = 'rep15sGood';

      var thresholdsDefined = {};
      var defaultThresholds = {};
      defaultThresholds.PercentageAnsweredBefore15 = {
        l : {lev: 0, style: 'rep15sBad'},m : {lev: 80, style : 'rep15sAverage' },h: {lev: 90, style : 'rep15sGood' }
      };
      defaultThresholds.PercentageAbandonnedAfter15 = {
        l : {lev: 0, style: 'abn15sGood'},m : {lev: 2, style : 'abn15sAverage' },h: {lev: 3, style : 'abn15sBad' }
      };
      defaultThresholds.WaitingCalls = {
        l : {lev: 0, style: 'waitingGood'},m : {lev: 1, style : 'waitingBad' },h: {lev: 3, style : 'waitingBad' }
      };
      defaultThresholds.LongestWaitTime = {
        l : {lev: 0, style: 'waitingTimeGood'},m : {lev: 1, style : 'waitingTimeBad' },h: {lev: 3, style : 'waitingTimeBad' }
      };
      defaultThresholds.AvailableAgents = {
        l : {lev: 0, style: 'agtDispoBad'}, m : {lev: 0, style : 'agtDispoAverage' },h: {lev: 3, style : 'agtDispoGood' }
      };

      var plThresholds = {};
      plThresholds.PercentageAnsweredBefore15 = {
        l : {lev: 0, style: 'rep15sBad'}, m : {lev: 80, style : 'rep15sAverage' },h: {lev: 90, style : 'rep15sGood' }
      };
      plThresholds.PercentageAbandonnedAfter15 = {
        l : {lev: 0, style: 'abn15sGood'}, m : {lev: 2, style : 'abn15sAverage' }, h: {lev: 3, style : 'abn15sBad' }
      };
      plThresholds.WaitingCalls = {
        l : {lev: 0, style: 'waitingGood'},m : {lev: 1, style : 'waitingBad' },h: {lev: 3, style : 'waitingBad' }
      };
      plThresholds.LongestWaitTime = {
        l : {lev: 0, style: 'waitingTimeGood'}, m : {lev: 1, style : 'waitingTimeBad' },h: {lev: 3, style : 'waitingTimeBad' }
      };
      plThresholds.AvailableAgents = {
        l : {lev: 0, style: 'agtDispoBad'},m : {lev: 0, style : 'agtDispoAverage' },h: {lev: 3, style : 'agtDispoGood' }
      };

      thresholdsDefined['st-'] = plThresholds;
      thresholdsDefined['pl-'] = plThresholds;

    var thresholds = defaultThresholds;

    var calculateStyle = function(name, value) {
      if (typeof(value) === 'undefined') return '';
      if (isNaN(value)) return '';
      if (value > thresholds[name].h.lev) {
        return thresholds[name].h.style;
      }
      if (value > thresholds[name].m.lev) {
        return thresholds[name].m.style;
      };
      return thresholds[name].l.style;
    };
    var _getStyle = function(name, value) {
      if (typeof(thresholds[name]) !== 'undefined') {
        return calculateStyle(name, value);
      }
      else {
        return defaultStyle;
      };
    };
    var _useThreshold = function(name) {
      console.log('setting thresholds '+ name+ ' ' + typeof(thresholdsDefined[name]));
      if (typeof(thresholdsDefined[name]) !== 'undefined') {
        thresholds  = thresholdsDefined[name];
      }
    };
    return {
      getStyle : _getStyle,
      useThreshold : _useThreshold
    }
}]);

  optpanel.controller('queueTotalController',
    ['XucQueue','$scope','thresholdStyles',function(xucQueue,$scope, thresholdStyles) {

      $scope.totals = {};
      $scope.totals.classes = {};
      $scope.maxLongestWaitTime = 0;
      thresholdStyles.useThreshold($scope.queueFilter);

      var initTotals = function() {
        $scope.totals.TotalNumberCallsEntered = 0;
        $scope.totals.TotalNumberCallsAnswered = 0;
        $scope.totals.TotalNumberCallsAbandonned = 0;
        $scope.totals.TotalNumberCallsClosed = 0;
        $scope.totals.WaitingCalls = 0;
        $scope.totals.TotalNumberCallsAnsweredBefore15 = 0;
        $scope.totals.TotalNumberCallsAbandonnedAfter15 = 0;
        $scope.totals.GlobalPercentageAnsweredBefore15 = 0;
        $scope.totals.GlobalPercentageAbandonnedAfter15 = 0;
        $scope.maxLongestWaitTime = 0;
      };

      $scope.getStyle = function(name, value) {
        return thresholdStyles.getStyle(name,value);
      };

      var updateTotals = function(queue) {
        angular.forEach(queue, function(value, property){
          if (typeof($scope.totals[property]) !== 'undefined') {
            $scope.totals[property] = $scope.totals[property] + value;
          }
          if (property === 'LongestWaitTime' && value !== '-') {
            if (value > $scope.maxLongestWaitTime)
              $scope.maxLongestWaitTime = value;
          }
        });
      };

      var calculatePercentages = function() {
        if ($scope.totals.TotalNumberCallsEntered > 0) {
          $scope.totals.GlobalPercentageAnsweredBefore15 = ($scope.totals.TotalNumberCallsAnsweredBefore15  / $scope.totals.TotalNumberCallsEntered)*100;
          $scope.totals.GlobalPercentageAbandonnedAfter15 = ($scope.totals.TotalNumberCallsAbandonnedAfter15  / $scope.totals.TotalNumberCallsEntered)*100;
        }
      };

      $scope.$watch('queues', function(newQueues, oldQueues) {
        initTotals();
        angular.forEach($scope.queues, function(queue, key){
          updateTotals(queue);
        });
        calculatePercentages();
      },true);

      initTotals();
      calculatePercentages();

  }]);


  optpanel.controller('agentController',['XucAgent','$scope','$timeout','ArrDispatcher','$filter',
      function(xucAgent, $scope, $timeout, arrDispatcher, $filter) {
      console.log('agentController ' + $scope.queueFilter);

      var agentStateTimer = 2;
      var agentStateWarningThreshold = 240;
      var agentStateWarningStyle = 'AgentStateWarning';
      $scope.agents = xucAgent.getAgentsInQueues($scope.queueFilter);
      $scope.agentsInCol = [];

      $scope.$on('ctiLoggedOn', function(e) {
          xucAgent.start();
      });

      var onAgentsUpdated = function() {
          $scope.agents = xucAgent.getAgentsInQueues($scope.queueFilter);
          $scope.agents = $filter('filter')($scope.agents, { state :'!loggedout'});
          $scope.agents = $filter('orderBy')($scope.agents, 'firstName', false);
          angular.forEach($scope.agents, function(agent, key) {
            agent.name = agent.firstName + ' ' + agent.lastName;
          });

          $scope.agentsInCol = arrDispatcher.dispatch($scope.agents,4);
      };
      $scope.$on('QueueMemberUpdated', function() {
        onAgentsUpdated();
      });
      $scope.$on('AgentsLoaded', function() {
        onAgentsUpdated();
      });
      $scope.$on('AgentStateUpdated', function() {
        onAgentsUpdated();
      });


      $scope.$on('linkDisConnected', function(e) {
          $scope.agents.length = 0;
          $scope.agentsInCol.length = 0;
      });

      var updateAgentState = function(agent) {
          if (agent.state !== 'AgentLoggedOut') {
              agent.timeInState = moment().countdown(agent.momentStart);
          } else {
              agent.timeInState = moment().add('seconds', 0);
              agent.phoneNb = '';
          }
      };
      $scope.updateAgentStates = function() {
          angular.forEach($scope.agentsInCol, function(line, key) {
            angular.forEach(line, function(agent, key) {
              updateAgentState(agent);
            });
          });
          $scope.$digest();
          $timeout($scope.updateAgentStates, agentStateTimer * 1000, false);
      };
      $timeout($scope.updateAgentStates, 1000, false);

      $scope.getStateStyle = function(stateTimer) {
        if(Math.abs(stateTimer.value) / 1000 > agentStateWarningThreshold) {
          return agentStateWarningStyle;
        }

      };

  }]);

optpanel.controller('ctiLinkController', function($rootScope, $scope, $timeout) {
  console.log('ctiLinkController');
    $scope.reconnectTimeout = 20000;
    $scope.clockFrequency = 50;
    $scope.connected = false;

    $scope.loggedOnHandler = function(event) {
        $rootScope.$broadcast('ctiLoggedOn');
    };

    $scope.linkStatusHandler = function(event) {
        console.log('link status : ' + event.status);
        $scope.status = event.status;
        if (event.status !== 'opened') {
            $rootScope.$broadcast('linkDisConnected');
            $timeout($scope.reconnect, $scope.reconnectTimeout);
        }
        $scope.$digest();
    };

    $scope.reconnect = function(event) {
        console.log('trying to reconnect');
        initCtiLocale(supervisionUsername);
    };

    // timeout
    $scope.updateClock = function() {
        $scope.clock = moment().format('H:mm:ss');
        $timeout($scope.updateClock, $scope.clockFrequency * 1000);
    };
    $timeout($scope.updateClock, $scope.clockFrequency * 1000);

    Cti.setHandler(Cti.MessageType.LOGGEDON, $scope.loggedOnHandler);
    Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, $scope.linkStatusHandler);
});

var supervisionUsername = '';
var initCtiLocale = function(username) {
    supervisionUsername = username;
    var password = '0000';
    var phoneNumber = '0';
    var host = '128.1.140.234:8080';
    host = 'localhost:9000';
    var wsurl = 'ws://'+host+'/xuc/ctichannel?username=' + supervisionUsername + '&amp;agentNumber=' + phoneNumber + '&amp;password=' + password;

    Cti.debugMsg = true;
    Cti.WebSocket.init(wsurl, supervisionUsername, 0);
};
