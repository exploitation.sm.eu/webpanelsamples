'use strict';

var lfm = angular.module('lfm', ['xuc.services'])
 .config(function($locationProvider) { $locationProvider.html5Mode(true); });

  lfm.controller('queueController',
    ['XucQueue','$scope','$timeout','$filter','$location',
    function(xucQueue, $scope, $timeout, $filter, $location) {
    var updateQueueFrequency = 5;
    $scope.queueFilter = $location.search().filter;
    $scope.queueIds = [1,4,5,6,7,8,9,76];

    $scope.queues = [];

    $scope.getQueues = function() {
        xucQueue.updateQueues(updateQueueFrequency);
        $scope.queues = xucQueue.getQueueByIds($scope.queueIds);
        $timeout($scope.getQueues,updateQueueFrequency*1000,false);
        $scope.$digest();
    };
    $timeout($scope.getQueues,1000,false);


    $scope.$on('ctiLoggedOn', function() {
        xucQueue.start();
    });
    $scope.$on('linkDisConnected', function() {
        console.log('queueController linkDisConnected');
        $scope.queues = [];
    });

  }]);
lfm.factory('ArrDispatcher',[function(){

  var dispatched = [];
  var rec = 0;

  var consoleAgents= function(agents) {
    console.log('agents : '+ agents.length);

    angular.forEach(agents, function(agent, key) {
      console.log(key + ' ' + agent.firstName+ ' ' + agent.phoneNb);
    });
  };

  var initDispatch = function(nbLines) {
    dispatched = [];
    for (var i=0; i < nbLines; i++ ) {
      dispatched.push([]);
    }
  };
  var getNbLines = function(nbOfElt, nbOfCol) {
    var nbLines = Math.floor((nbOfElt/nbOfCol));
    if (( nbOfElt % nbOfCol) > 0) {
      nbLines += 1;
    }
    return nbLines;
  };

  var doDispatch = function(arr,nbOfCol) {
    rec = rec + 1;
    if (rec > 10000) {
      console.error('maximum recursive('+ rec +') attempt to dispatch agents nb of col : '+ nbOfCol);
      consoleAgents(arr);
      return;
    }
    if (arr.length === 0) {
      return;
    }

    var nbLines = getNbLines(arr.length, nbOfCol);
    for (var i = 0; i < nbLines; i++) {
      dispatched[i].push(arr[i]);
    }
    arr.splice(0,nbLines);
    doDispatch(arr,nbOfCol-1);
  };

  var _dispatch = function(arr, nbOfCol) {
    rec = 0;
    var nbLines = getNbLines(arr.length, nbOfCol);
    initDispatch(nbLines);

    doDispatch(arr,nbOfCol);
    return dispatched;
  };

  return {
    dispatch : _dispatch
  };
}]);

lfm.controller('queueTotalController',
    ['XucQueue','$scope',function(xucQueue,$scope) {

      $scope.totals = {};
      $scope.totals.classes = {};
      $scope.maxLongestWaitTime = 0;

      var initTotals = function() {
        $scope.totals.TotalNumberCallsEntered = 0;
        $scope.totals.TotalNumberCallsEnteredLastHour = 0;
        $scope.totals.TotalNumberCallsAnswered = 0;
        $scope.totals.TotalNumberCallsAnsweredLastHour = 0;
        $scope.totals.TotalNumberCallsAbandonned = 0;
        $scope.totals.TotalNumberCallsTimeout = 0;
        $scope.totals.WaitingCalls = 0;
        $scope.totals.GlobalPercentageAnswered = 0;
        $scope.totals.GlobalPercentageAnsweredLastHour = 0;
      };

      var updateTotals = function(queue) {
        angular.forEach(queue, function(value, property){
          if (typeof($scope.totals[property]) !== 'undefined') {
            $scope.totals[property] = $scope.totals[property] + value;
          }
        });
      };

      var calculatePercentages = function() {
        if ($scope.totals.TotalNumberCallsEntered > 0) {
          $scope.totals.GlobalPercentageAnswered = ($scope.totals.TotalNumberCallsAnswered  / $scope.totals.TotalNumberCallsEntered)*100;
          $scope.totals.GlobalPercentageAnsweredLastHour = ($scope.totals.TotalNumberCallsAnsweredLastHour  / $scope.totals.TotalNumberCallsEnteredLastHour)*100;
        }
      };

      $scope.bravo = function() {
        return Math.round($scope.totals.GlobalPercentageAnswered) >= 90;
      };
      $scope.$watch('queues', function(newQueues, oldQueues) {
        initTotals();
        angular.forEach($scope.queues, function(queue, key){
          updateTotals(queue);
        });
        calculatePercentages();
      },true);

      initTotals();
      calculatePercentages();

  }]);


  lfm.controller('agentController',['XucAgent','$scope','$timeout','ArrDispatcher','$filter',
      function(xucAgent, $scope, $timeout, arrDispatcher, $filter) {
      console.log('agentController ' + $scope.queueFilter);

      var agentStateTimer = 2;
      var agentStateWarningThreshold = 240;
      var agentStateWarningStyle = 'AgentStateWarning';
      var nbOfColumns = 3;
      $scope.agents = xucAgent.getAgentsInQueueByIds($scope.queueIds);
      $scope.agentsInCol = [];

      $scope.$on('ctiLoggedOn', function(e) {
          xucAgent.start();
      });

      var onAgentsUpdated = function() {
          $scope.agents = xucAgent.getAgentsInQueueByIds($scope.queueIds);
          $scope.agents = $filter('orderBy')($scope.agents, 'firstName', false);
          angular.forEach($scope.agents, function(agent, key) {
            agent.name = agent.firstName + ' ' + agent.lastName;
          });

          $scope.agentsInCol = arrDispatcher.dispatch($scope.agents,nbOfColumns);
      };
      $scope.$on('QueueMemberUpdated', function() {
        onAgentsUpdated();
      });
      $scope.$on('AgentsLoaded', function() {
        onAgentsUpdated();
      });
      $scope.$on('AgentStateUpdated', function() {
        onAgentsUpdated();
      });


      $scope.$on('linkDisConnected', function(e) {
          $scope.agents.length = 0;
          $scope.agentsInCol.length = 0;
      });

      var updateAgentState = function(agent) {
          if (agent.state !== 'AgentLoggedOut') {
              agent.timeInState = moment().countdown(agent.momentStart);
          } else {
              agent.timeInState = moment().add('seconds', 0);
              agent.phoneNb = '';
          }
      };
      $scope.updateAgentStates = function() {
          angular.forEach($scope.agentsInCol, function(line, key) {
            angular.forEach(line, function(agent, key) {
              updateAgentState(agent);
            });
          });
          $scope.$digest();
          $timeout($scope.updateAgentStates, agentStateTimer * 1000, false);
      };
      $timeout($scope.updateAgentStates, 1000, false);

      $scope.getStateStyle = function(stateTimer) {
        if(Math.abs(stateTimer.value) / 1000 > agentStateWarningThreshold) {
          return agentStateWarningStyle;
        }

      };

  }]);

var supervisionUsername = '';
var initCtiLocale = function(username) {
    supervisionUsername = username;
    var password = '0000';
    var phoneNumber = '0';
    var host =  window.location.hostname+":9000";
    var wsurl = 'ws://'+host+'/xuc/ctichannel?username=' + supervisionUsername + '&amp;agentNumber=' + phoneNumber + '&amp;password=' + password;

    Cti.debugMsg = false;
    Cti.WebSocket.init(wsurl, supervisionUsername, 0);
};

lfm.controller('ctiLinkController', function($rootScope, $scope, $timeout) {
  console.log('ctiLinkController');
    $scope.reconnectTimeout = 20000;
    $scope.clockFrequency = 1;
    $scope.connected = false;
    $scope.currentDay = '--/--/----';
    $scope.clock = '--:--:--';

    $scope.loggedOnHandler = function(event) {
        $rootScope.$broadcast('ctiLoggedOn');
    };

    $scope.linkStatusHandler = function(event) {
        console.log('link status : ' + event.status);
        $scope.status = event.status;
        if (event.status !== 'opened') {
            $rootScope.$broadcast('linkDisConnected');
            $timeout($scope.reconnect, $scope.reconnectTimeout);
            $scope.connected = false;
        }
        else {
          $scope.connected = true;
        }
        $scope.$digest();
    };

    $scope.reconnect = function(event) {
        console.log('trying to reconnect');
        initCtiLocale(supervisionUsername);
    };

    // timeout
    $scope.updateClock = function() {
        $scope.currentDay = moment().format('DD/MM/YYYY');
        $scope.clock = moment().format('HH:mm:ss');
        $timeout($scope.updateClock, $scope.clockFrequency * 1000);
    };
    $timeout($scope.updateClock, $scope.clockFrequency * 1000);

    Cti.setHandler(Cti.MessageType.LOGGEDON, $scope.loggedOnHandler);
    Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, $scope.linkStatusHandler);
});

