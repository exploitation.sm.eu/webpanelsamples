'use strict';

describe('Service: arraydispatcher', function () {
  var arrDispatcher;

  // load the controller's module
  beforeEach(module('optpanel'));

    beforeEach(inject(function(_ArrDispatcher_) {
        arrDispatcher = _ArrDispatcher_;
    }));

    it('should be able to transform an array of nbofcol elt in multicolumn', function(){

      var arr = [1,2,3,4];

      var dispatched = arrDispatcher.dispatch(arr,4);

      expect(dispatched).toEqual([[1,2,3,4]]);
    });

    it('should be able to transform an array of nb of elt % nbcol =0 in multicolumn', function(){

      var arr = [1,2,3,4,5,6,7,8];

      var dispatched = arrDispatcher.dispatch(arr,4);

      expect(dispatched).toEqual([[1,3,5,7],[2,4,6,8]]);

    });
    it('should be able to transform an array of nb of elt in multicolumn', function(){

      var arr = [1,2,3,4,5,6,7,8,9];

      var dispatched = arrDispatcher.dispatch(arr,4);

      expect(dispatched).toEqual([[1,4,6,8],[2,5,7,9], [3]]);

    });

    it('should be able to transform an array of 10 elt in multicolumn', function(){

      var arr = [1,2,3,4,5,6,7,8,9,10];

      var dispatched = arrDispatcher.dispatch(arr,4);

      expect(dispatched).toEqual([[1,4,7,9],[2,5,8,10], [3,6]]);

    });
});
