'use strict';

describe('Controller: queueTotalController', function () {

  // load the controller's module
  beforeEach(module('optpanel'));

  var AboutCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AboutCtrl = $controller('queueTotalController', {
      $scope: scope
    });
  }));

  it('should have empty totals', function () {
  /*
      [
      {"id":170,"context":"default","name":"bluesky","displayName":"Blue Sky","number":"3012","LongestWaitTime":"-","WaitingCalls":0,"AvailableAgents":1,"LoggedAgents":8,"TalkingAgents":0,"EWT":0,"$$hashKey":"00Y"},
      {"id":1,"context":"default","name":"blue","displayName":"Blue Ocean","number":"3000",
        "LongestWaitTime":"-","WaitingCalls":0,"AvailableAgents":2,"LoggedAgents":9,"TalkingAgents":0,"EWT":9,"$$hashKey":"00X",
        "TotalNumberCallsClosed":2,"PercentageAnsweredBefore15":0,"PercentageAbandonnedAfter15":0,"TotalNumberCallsEntered":2,"TotalNumberCallsAbandonned":1,"TotalNumberCallsAnswered":1}]
  */

    expect(scope.totals.TotalNumberCallsEntered).toBe(0);
    expect(scope.totals.TotalNumberCallsAnswered).toBe(0);
    expect(scope.totals.TotalNumberCallsAbandonned).toBe(0);
    expect(scope.totals.TotalNumberCallsClosed).toBe(0);
    expect(scope.totals.WaitingCalls).toBe(0);
    expect(scope.totals.TotalNumberCallsAnsweredBefore15).toBe(0);
    expect(scope.totals.TotalNumberCallsAbandonnedAfter15).toBe(0);
    expect(scope.totals.GlobalPercentageAnsweredBefore15).toBe(0);
    expect(scope.totals.GlobalPercentageAbandonnedAfter15).toBe(0);


  });

  it('should accumulate totals on queue changed', function(){
    var q1 = {"id":1,"WaitingCalls":5,"TotalNumberCallsClosed":2,"TotalNumberCallsEntered":2,"TotalNumberCallsAbandonned":7,"TotalNumberCallsAnswered":4}
    var q2 = {"id":2,"WaitingCalls":4,"TotalNumberCallsClosed":0,"TotalNumberCallsEntered":1,"TotalNumberCallsAbandonned":4,"TotalNumberCallsAnswered":8}

    scope.queues = [q1,q2];
    scope.$digest();

    expect(scope.totals['TotalNumberCallsEntered']).toBe(3);

  });
  it('should calculate max longestWaitTime on queue changed', function(){
    var q1 = {"id":1,"WaitingCalls":5,"LongestWaitTime":2}
    var q2 = {"id":2,"WaitingCalls":4,"LongestWaitTime":'-'}
    var q3 = {"id":1,"WaitingCalls":5,"LongestWaitTime":20}

    scope.queues = [q1,q2,q3];
    scope.$digest();

    expect(scope.maxLongestWaitTime).toBe(20);

  });
  it('should reset max longestWaitTime on no longest wait time', function(){
    var q1 = {"id":3,"WaitingCalls":5,"LongestWaitTime":2}
    var q2 = {"id":4,"WaitingCalls":5,"LongestWaitTime":15,}
    scope.queues = [q1,q2];
    scope.$digest();

    var q1 = {"id":3,"WaitingCalls":5,"LongestWaitTime":'-'}
    var q2 = {"id":4,"WaitingCalls":5,"LongestWaitTime":'-',}

    scope.queues = [q1,q2];
    scope.$digest();

    expect(scope.maxLongestWaitTime).toBe(0);

  });

  it('should reset totals between updateds', function() {
    var q1 = {"id":1,"WaitingCalls":5,"TotalNumberCallsClosed":2,"TotalNumberCallsEntered":2,"TotalNumberCallsAbandonned":7,"TotalNumberCallsAnswered":4}
    scope.queues = [q1];
    scope.$digest();
    q1 = {"id":1,"WaitingCalls":2,"TotalNumberCallsClosed":2,"TotalNumberCallsEntered":2,"TotalNumberCallsAbandonned":7,"TotalNumberCallsAnswered":4}
    scope.queues = [q1];
    scope.$digest();

    expect(scope.totals['TotalNumberCallsEntered']).toBe(2);
  });
  it('should accumulate totals when only one queue changes', function(){
    var q1 = {"id":11,"WaitingCalls":5,"TotalNumberCallsClosed":2,"TotalNumberCallsEntered":2,"TotalNumberCallsAbandonned":7,"TotalNumberCallsAnswered":4}
    var q2 = {"id":22,"WaitingCalls":4,"TotalNumberCallsClosed":0,"TotalNumberCallsEntered":7,"TotalNumberCallsAbandonned":4,"TotalNumberCallsAnswered":8}
    scope.queues = [q1,q2];
    scope.$digest();

    scope.queues[0].TotalNumberCallsEntered = 5;
    scope.$digest();

    expect(scope.totals['TotalNumberCallsEntered']).toBe(12);

  });
  it('should calculate a global percentage of calls answered before 15s', function(){
    var q1 = {"id":54,"TotalNumberCallsEntered":40,"TotalNumberCallsAnsweredBefore15":5}
    var q2 = {"id":55,"TotalNumberCallsEntered":20,"TotalNumberCallsAnsweredBefore15":10}

    scope.queues = [q1,q2];
    scope.$digest();

    expect(scope.totals.GlobalPercentageAnsweredBefore15).toBe(25.0);

  });
  it('should calculate a global percentage of zero if no denomitaor ', function(){
    var q1 = {"id":54,"WaitingCalls":5}

    scope.queues = [q1];
    scope.$digest();

    expect(scope.totals.GlobalPercentageAnsweredBefore15).toBe(0);
    expect(scope.totals.GlobalPercentageAbandonnedAfter15).toBe(0);

  });
  it('should ')
});
