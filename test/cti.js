/*
* TODO: {"class": "ipbxcommand", "command": "listen", "commandid": 363963032, "destination": "xivo/19", "subcommand": "start"}
*
*/
var Cti = {
    debugMsg : false,
    debugHandler : false,

    init : function(username, agentNumber, webSocket) {
        this.username = username;
        this.agentNumber = agentNumber;
        this.sendCallback = webSocket.sendCallback;
        this.webSocket = webSocket;
    },

    close: function() {
        this.webSocket.close();
    },

    Topic : function(id) {
        var callbacks, topic = id && Cti.ctiTopics[id];
        if (!topic) {
            callbacks = jQuery.Callbacks();
            topic = {
                publish : callbacks.fire,
                subscribe : callbacks.add,
                unsubscribe : callbacks.remove
            };
            if (id) {
                Cti.ctiTopics[id] = topic;
            }
        }
        return topic;
    },

    setHandler : function(eventName, handler) {
        Cti.Topic(eventName).subscribe(handler);
        if (Cti.debugHandler) {
            console.log("subscribing : [" + eventName + "] to " + handler);
        }
    },

    unsetHandler : function(eventName, handler) {
        Cti.Topic(eventName).unsubscribe(handler);
        if (Cti.debugHandler) {
            console.log("unsubscribing : [" + eventName + "] to " + handler);
        }
    },

    receive : function(event) {
        var message = this.getMessage(event.data);
        if (message === null) {
            console.log("WARNING: No message in decoded json data: " + data);
            throw new TypeError("No message in decoded json data");
        }
        if (Cti.debugMsg)
            console.log("R<<< " + JSON.stringify(message));
        Cti.Topic(message.msgType).publish(message.ctiMessage);
        Cti.msgReceived ++;
    },

    getMessage : function(jsonData) {
        try {
            return JSON.parse(jsonData);
        } catch (err) {
            console.log("ERROR: " + err + ", event.data not json encoded: " + jsonData);
            throw new TypeError("Json parse of received data failed");
        }
    },

    processUserStatusUpdate : function(userStatus) {
        $.each(this.userStatuses, function(key, item) {
            if (item.name == userStatus.status) {
                $("#currentPresence").html(item.longName);
                $("#statusBar").css("background-image", "linear-gradient(" + item.color + ",rgb(255, 255, 255))");
            }
        });
    },

    processUsersStatuses : function(statuses) {
        this.userStatuses = statuses;
        $.each(this.userStatuses, function(key, item) {
            $("#userPresence").append("<li><a href=\"#\" id=\"" + item.name + "\">" + item.longName + "</a></li>");
        });
        $('.dropdown-menu li a').click(function(event) {
            Cti.changeUserStatus($(this).attr('id'));
        });
    },

    processDirectoryResult : function(directoryResult) {
        DirectoryDisplay.prepareTable();
        DirectoryDisplay.displayHeaders(directoryResult.headers);
        DirectoryDisplay.displayEntries(directoryResult.entries);
    },
    sendPing : function() {
        console.log("rec "+ Cti.msgReceived + " " + Cti.ctiChannelSocket);
        Cti.msgReceived = 0;
        var message = Cti.WebsocketMessageFactory.createPing();
        Cti.sendCallback(message);
    },
    changeUserStatus : function(statusId) {
        var message = Cti.WebsocketMessageFactory.createUserStatusUpdate(statusId);
        this.sendCallback(message);
    },
    loginAgent : function(agentPhoneNumber, agentId) {
        var message = Cti.WebsocketMessageFactory.createAgentLogin(agentPhoneNumber, agentId);
        this.sendCallback(message);
    },
    logoutAgent : function(agentId) {
        var message = Cti.WebsocketMessageFactory.createAgentLogout(agentId);
        this.sendCallback(message);
    },
    pauseAgent : function(agentId) {
        var message = Cti.WebsocketMessageFactory.createPauseAgent(agentId);
        this.sendCallback(message);
    },
    unpauseAgent : function(agentId) {
        var message = Cti.WebsocketMessageFactory.createUnpauseAgent(agentId);
        this.sendCallback(message);
    },
    listenAgent : function(agentId) {
        var args ={'agentid' : agentId};
        var message = Cti.WebsocketMessageFactory.createMessageFromArgs(Cti.WebsocketMessageFactory.listenAgentCmd, args);
        this.sendCallback(message);
    },
    dnd : function(state) {
        var message = Cti.WebsocketMessageFactory.createDnd(state);
        this.sendCallback(message);
    },
    dial : function(destination) {
        var message = Cti.WebsocketMessageFactory.createDial(destination);
        this.sendCallback(message);
    },
    originate : function(destination) {
        var message = Cti.WebsocketMessageFactory.createOriginate(destination);
        this.sendCallback(message);
    },
    hangup : function() {
        var message = Cti.WebsocketMessageFactory.createHangup();
        this.sendCallback(message);
    },
    answer : function() {
        var message = Cti.WebsocketMessageFactory.createAnswer();
        this.sendCallback(message);
    },
    hold : function() {
        var message = Cti.WebsocketMessageFactory.createHold();
        this.sendCallback(message);
    },
    directTransfer : function(destination) {
        var message = Cti.WebsocketMessageFactory.createDirectTransfer(destination);
        this.sendCallback(message);
    },
    attendedTransfer : function(destination) {
        var message = Cti.WebsocketMessageFactory.createAttendedTransfer(destination);
        this.sendCallback(message);
    },
    completeTransfer : function() {
        var message = Cti.WebsocketMessageFactory.createCompleteTransfer();
        this.sendCallback(message);
    },
    cancelTransfer : function() {
        var message = Cti.WebsocketMessageFactory.createCancelTransfer();
        this.sendCallback(message);
    },
    conference : function() {
        var message = Cti.WebsocketMessageFactory.createConference();
        this.sendCallback(message);
    },
    searchDirectory : function(pattern) {
        var message = Cti.WebsocketMessageFactory.createSearchDirectory(pattern);
        this.sendCallback(message);
    },
    subscribeToQueueStats : function() {
        var message = Cti.WebsocketMessageFactory.createSubscribeToQueueStats();
        this.sendCallback(message);
    },
    getQueueStatistics : function(queueId, window, xqos) {
        var message = Cti.WebsocketMessageFactory.createGetQueueStatistics(queueId, window, xqos);
        this.sendCallback(message);
    },
    subscribeToAgentEvents : function() {
        var message = Cti.WebsocketMessageFactory.createSubscribeToAgentEvents();
        this.sendCallback(message);
    },
    getAgentStates : function() {
        var message = Cti.WebsocketMessageFactory.createGetAgentStates();
        this.sendCallback(message);
    },
    getConfig : function(objectType) {
        var message = Cti.WebsocketMessageFactory.createGetConfig(objectType);
        this.sendCallback(message);
    },
    getList : function(objectType) {
        var message = Cti.WebsocketMessageFactory.createGetList(objectType);
        this.sendCallback(message);
    },
    getAgentDirectory : function() {
        var message = Cti.WebsocketMessageFactory.createGetAgentDirectory();
        this.sendCallback(message);
    },
    setAgentQueue : function(agentId, queueId, penalty) {
        var message = Cti.WebsocketMessageFactory.createSetAgentQueue(agentId, queueId, penalty);
        this.sendCallback(message);
    },
    removeAgentFromQueue : function(agentId,queueId) {
        var message = Cti.WebsocketMessageFactory.createRemoveAgentFromQueue(agentId,queueId);
        this.sendCallback(message);
    },
    moveAgentsInGroup : function(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty) {
        var args = {};
        args.groupId = groupId;
        args.fromQueueId = fromQueueId;
        args.fromPenalty = fromPenalty;
        args.toQueueId = toQueueId;
        args.toPenalty = toPenalty;

        var message = Cti.WebsocketMessageFactory.createMessageFromArgs(Cti.WebsocketMessageFactory.moveAgentsInGroupCmd, args);
        this.sendCallback(message);

    },
    addAgentsInGroup : function(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty) {
        var args = {};
        args.groupId = groupId;
        args.fromQueueId = fromQueueId;
        args.fromPenalty = fromPenalty;
        args.toQueueId = toQueueId;
        args.toPenalty = toPenalty;

        var message = Cti.WebsocketMessageFactory.createMessageFromArgs(Cti.WebsocketMessageFactory.addAgentsInGroupCmd, args);
        this.sendCallback(message);
    },
    removeAgentGroupFromQueueGroup: function(groupId, queueId, penalty) {
        var args ={'groupId' : groupId, 'queueId' : queueId, 'penalty' : penalty};

        var message = Cti.WebsocketMessageFactory.createMessageFromArgs(Cti.WebsocketMessageFactory.removeAgentGroupFromQueueGroupCmd, args);
        this.sendCallback(message);
    },
    addAgentsNotInQueueFromGroupTo: function(groupId, queueId, penalty) {
        var args ={'groupId' : groupId, 'queueId' : queueId, 'penalty' : penalty};

        var message = Cti.WebsocketMessageFactory.createMessageFromArgs(Cti.WebsocketMessageFactory.addAgentsNotInQueueFromGroupToCmd, args);
        this.sendCallback(message);

    },

};
Cti.ctiTopics = {};

Cti.MessageType = {
    ERROR : "Error",
    LOGGEDON : "LoggedOn",
    SHEET : "Sheet",
    USERSTATUSES : "UsersStatuses",
    USERSTATUSUPDATE : "UserStatusUpdate",
    DIRECTORYRESULT : "DirectoryResult",
    PHONESTATUSUPDATE : "PhoneStatusUpdate",
    LINKSTATUSUPDATE : "LinkStatusUpdate",
    QUEUESTATISTICS : "QueueStatistics",
    QUEUECONFIG : "QueueConfig",
    QUEUELIST : "QueueList",
    QUEUEMEMBER : "QueueMember",
    QUEUEMEMBERLIST : "QueueMemberList",
    GETQUEUESTATISTICS : "GetQueueStatistics",
    AGENTSTATEEVENT : "AgentStateEvent",
    AGENTCONFIG : "AgentConfig",
    AGENTLIST : "AgentList",
    AGENTGROUPLIST : "AgentGroupList",
    AGENTERROR : "AgentError",
    AGENTDIRECTORY : "AgentDirectory"
};

Cti.PhoneStatus = {
    ONHOLD : 16,
    RINGING : 8,
    INDISPONIBLE : 4,
    BUSY_AND_RINGING : 9,
    AVAILABLE : 0,
    CALLING : 1,
    BUSY : 2,
    DEACTIVATED : -1,
    UNEXISTING : -2,
    ERROR : -99
};

Cti.PhoneStatusColors = {
    "16" : "#F7FE2E",
    "8" : "#2E2EFE",
    "4" : "#F2F2F2",
    "9" : "#CC2EFA",
    "0" : "#01DF01",
    "1" : "#FF8000",
    "2" : "#81BEF7",
    "-1" : "#F2F2F2",
    "-2" : "#F2F2F2",
    "-99" : "#F2F2F2"
};

Cti.WebsocketMessageFactory = {

    messageClaz : "web",
    pingClaz : "ping",
    agentLoginCmd : "agentLogin",
    agentLogoutCmd : "agentLogout",
    userStatusUpdateCmd : "userStatusUpdate",
    dndCmd : "dnd",
    dialCmd : "dial",
    originateCmd : "originate",
    searchDirectoryCmd : "searchDirectory",
    pauseAgentCmd : "pauseAgent",
    unpauseAgentCmd : "unpauseAgent",
    listenAgentCmd : "listenAgent",
    subscribeToQueueStatsCmd : "subscribeToQueueStats",
    hangupCmd : "hangup",
    answerCmd : "answer",
    holdCmd : "hold",
    directTransferCmd : "directTransfer",
    attendedTransferCmd : "attendedTransfer",
    completeTransferCmd : "completeTransfer",
    cancelTransferCmd : "cancelTransfer",
    conferenceCmd : "conference",
    getQueueStatisticsCmd : "getQueueStatistics",
    subscribeToAgentEventsCmd : "subscribeToAgentEvents",
    getAgentStatesCmd : "getAgentStates",
    getConfigCmd : "getConfig",
    getListCmd : "getList",
    getAgentDirectoryCmd : "getAgentDirectory",
    setAgentQueueCmd : "setAgentQueue",
    removeAgentFromQueueCmd : "removeAgentFromQueue",
    moveAgentsInGroupCmd : "moveAgentsInGroup",
    addAgentsInGroupCmd : "addAgentsInGroup",
    removeAgentGroupFromQueueGroupCmd : "removeAgentGroupFromQueueGroup",
    addAgentsNotInQueueFromGroupToCmd : 'addAgentsNotInQueueFromGroupTo',


    createAgentLogin : function(agentPhoneNumber, agentid) {
        var message = this.createMessage(this.agentLoginCmd);
        message.agentphonenumber = agentPhoneNumber;
        return this._createAgentMessage(message, agentid);
    },
    createAgentLogout : function(agentid) {
        return this._createAgentMessage(this.createMessage(this.agentLogoutCmd), agentid);
    },
    createPauseAgent : function(agentid) {
        return this._createAgentMessage(this.createMessage(this.pauseAgentCmd), agentid);
    },
    createUnpauseAgent : function(agentid) {
        return this._createAgentMessage(this.createMessage(this.unpauseAgentCmd), agentid);
    },
    _createAgentMessage : function(message, agentid) {
        message.agentid = agentid;
        return message;
    },
    createUserStatusUpdate : function(status) {
        var message = this.createMessage(this.userStatusUpdateCmd);
        message.status = status;
        return message;
    },
    createDnd : function(state) {
        var message = this.createMessage(this.dndCmd);
        message.state = state;
        return message;
    },
    createDial : function(destination) {
        return this.createDestinationMessage(this.dialCmd, destination);
    },
    createOriginate : function(destination) {
        return this.createDestinationMessage(this.originateCmd, destination);
    },
    createHangup : function() {
        return this.createMessage(this.hangupCmd);
    },
    createAnswer : function() {
        return this.createMessage(this.answerCmd);
    },
    createHold : function() {
        return this.createMessage(this.holdCmd);
    },
    createDirectTransfer : function(destination) {
        return this.createDestinationMessage(this.directTransferCmd, destination);
    },
    createAttendedTransfer : function(destination) {
        return this.createDestinationMessage(this.attendedTransferCmd, destination);
    },
    createCompleteTransfer : function() {
        return this.createMessage(this.completeTransferCmd);
    },
    createCancelTransfer : function() {
        return this.createMessage(this.cancelTransferCmd);
    },
    createConference : function() {
        return this.createMessage(this.conferenceCmd);
    },
    createSearchDirectory : function(pattern) {
        var message = this.createMessage(this.searchDirectoryCmd);
        message.pattern = pattern;
        return message;
    },
    createSubscribeToQueueStats : function() {
        return this.createMessage(this.subscribeToQueueStatsCmd);
    },
    createGetQueueStatistics : function(queueId, window, xqos) {
        var message = this.createMessage(this.getQueueStatisticsCmd);
        message.queueId = queueId;
        message.window = window;
        message.xqos = xqos;
        return message;
    },
    createSubscribeToAgentEvents : function() {
        return this.createMessage(this.subscribeToAgentEventsCmd);
    },
    createGetAgentStates : function() {
        return this.createMessage(this.getAgentStatesCmd);
    },
    createGetConfig : function(objectType) {
        var msg = this.createMessage(this.getConfigCmd);
        msg.objectType = objectType;
        return msg;
    },
    createGetList : function(objectType) {
        var msg = this.createMessage(this.getListCmd);
        msg.objectType = objectType;
        return msg;
    },
    createSetAgentQueue : function(agentId, queueId, penalty) {
        var msg = this.createMessage(this.setAgentQueueCmd);
        msg.agentId = agentId;
        msg.queueId = queueId;
        msg.penalty = penalty;
        return msg;
    },
    createRemoveAgentFromQueue : function(agentId,queueId) {
        var msg = this.createMessage(this.removeAgentFromQueueCmd);
        msg.agentId = agentId;
        msg.queueId = queueId;
        return msg;
    },
    createGetAgentDirectory : function(objectType) {
        return this.createMessage(this.getAgentDirectoryCmd);
    },
    createPing : function() {
        var message = {};
        message.claz = this.pingClaz;
        return message;
    },
    createDestinationMessage : function(command, destination) {
        var message = this.createMessage(command);
        message.destination = destination;
        return message;
    },
    createMessageFromArgs : function(command, args) {
        var msg = this.createMessage(command);
        for (var arg in args) {
            msg[arg] = args[arg];
        }
        return msg;
    },
    createMessage : function(command) {
        var message = {};
        message.claz = this.messageClaz;
        message.command = command;
        return message;
    }
};
Cti.WebSocket = (function() {
    var socketState = {};
    var missed_heartbeats = 0;
    var pingInterval = 15000;
    var heartbeat_interval = null;
    var heartbeat_msg = JSON.stringify(Cti.WebsocketMessageFactory.createPing());


    setSocketHandlers = function(socket) {
        socket.onopen = function() {
            socketState.status = "opened";
            Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).publish(socketState);
            if (heartbeat_interval === null) {
                missed_heartbeats = 0;
                heartbeat_interval = setInterval(function() {
                    try {
                        missed_heartbeats++;
                        console.log("ms :" + missed_heartbeats);
                        if (missed_heartbeats >= 3)
                            throw new Error("Too many missed heartbeats.");
                        socket.send(heartbeat_msg);
                    } catch(e) {
                        clearInterval(heartbeat_interval);
                        heartbeat_interval = null;
                        console.warn("Closing connection. Reason: " + e.message);
                        socket.close();
                    }
                }, pingInterval);
            }
        };

        socket.onclose = function() {
            if (heartbeat_interval !== null) {
                clearInterval(heartbeat_interval);
                heartbeat_interval = null;
            }
            socketState.status = "closed";
            Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).publish(socketState);
        };

        socket.onerror = function(error) {
            console.warn('ERROR: Error detected: ' + JSON.stringify(error));
        };

        socket.onmessage = function(event) {
            missed_heartbeats = 0;
            Cti.receive(event);
        };

        socket.sendCallback = function(message) {
            var jsonMessage = JSON.stringify(message);
            if (Cti.debugMsg) {
                console.log("S>>> " + jsonMessage);
            }
            socket.send(jsonMessage);
        };
    };

    return {
        init : function(wsurl, username, phoneNumber) {
            var WS = window.MozWebSocket ? MozWebSocket : WebSocket;
            var ctiChannelSocket = new WS(wsurl);
            setSocketHandlers(ctiChannelSocket);
            Cti.init(username, phoneNumber, ctiChannelSocket);
        }
    };
})();
